This is a simple microservice that shows how a web endpoint can be provided
for making MAPR queries to an OMERO database. Run in separate terminals:

ssh -L5555:localhost:5555 -o ServerAliveInterval=300 -At centos@idr-experimental.openmicroscopy.org ssh -L5555:192.168.49.5:5432 test50-omeroreadonly

java -Domero.db.host=localhost -Domero.db.port=5555 -Domero.db.name=idr -Domero.db.user=omeroreadonly -Domero.db.pass=ASK_ME -jar target/mapr-data-microservice-0.1.2-jar-with-dependencies.jar

src/scripts/post.py
then
wget 'http://localhost:8080/id?role=guest&type=group' -O response.json

An alternative server is:

java -jar target/mapr-data-microservice-0.1.2-jar-with-dependencies.jar etc/omero.properties
