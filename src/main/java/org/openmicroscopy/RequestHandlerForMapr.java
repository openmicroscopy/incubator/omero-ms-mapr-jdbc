/*
 * Copyright (C) 2018 University of Dundee & Open Microscopy Environment.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.openmicroscopy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * Provide MAPR data from JDBC.
 * @author m.t.b.carroll@dundee.ac.uk
 */
public class RequestHandlerForMapr {

    private final Connection connection;

    public RequestHandlerForMapr(Connection connection) {
        this.connection = connection;
    }

    /**
     * Add this as POST handler for the given path.
     * @param router the router for which this can handle requests
     * @param path the path on which those requests come
     */
    public void handleFor(Router router, String path) {
        router.post(path).handler(BodyHandler.create()).handler(getHttpHandler());
    }

     /**
     * Construct a HTTP failure response.
     * @param response the HTTP response that is to bear the failure
     * @param code the HTTP response code
     * @param message a message that describes the failure
     */
   private static void fail(HttpServerResponse response, int code, String message) {
       /* Copied from RequestHandlerForId. Could be moved to common utility class. */
        response.setStatusCode(code);
        response.setStatusMessage(message);
        response.end();
    }

    /**
     * Uses JDBC to answer the incoming query.
     * @param querySpecification the query represented in JSON
     * @return the query results from JDBC
     * @throws IllegalArgumentException if the query is not supported
     * @throws SQLException if the query attempt fails
     */
    private JsonArray runQuery(JsonObject querySpecification) throws SQLException {
        final JdbcQueryBuilder query = new JdbcQueryBuilder(querySpecification);
        final JsonArray results = new JsonArray();
        try (final PreparedStatement statement = query.createStatement(connection)) {
            final ResultSet rows = statement.executeQuery();
            final int columnCount = rows.getMetaData().getColumnCount();
            while (rows.next()) {
                final JsonArray resultRow = new JsonArray();
                for (int index = 1; index <= columnCount; index++) {
                    resultRow.add(rows.getObject(index));
                }
                results.add(resultRow);
            }
        }
        return results;
    }

    /**
     * @return handler for requests from HTTP endpoint
     */
    public Handler<RoutingContext> getHttpHandler() {
        return (final RoutingContext context) -> {
            final HttpServerRequest request = context.request();
            final HttpServerResponse response = request.response();
            final JsonObject body = context.getBodyAsJson();
            final JsonArray responseRows;
            try {
                responseRows = runQuery(body);
            } catch (IllegalArgumentException iae) {
                fail(response, 500, "query error: " + iae.getMessage());
                return;
            } catch (SQLException sqle) {
                fail(response, 500, "database query failed");
                return;
            }
            final String responseText = responseRows.toString();
            response.putHeader("Content-Type", "application/json; charset=utf-8");
            response.putHeader("Content-Length", Integer.toString(responseText.length()));
            response.end(responseText);
        };
    }

    /**
     * @return handler for requests received over Vert.x event bus
     */
    public Handler<Message<JsonObject>> getMessageHandler() {
        return (final Message<JsonObject> message) -> {
            final JsonObject body = message.body();
            try {
                message.reply(runQuery(body));
            } catch (IllegalArgumentException iae) {
                message.fail(500, "query error: " + iae.getMessage());
            } catch (SQLException sqle) {
                message.fail(500, "database query failed");
            }
        };
    }
}
