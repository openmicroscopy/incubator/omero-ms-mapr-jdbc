/*
 * Copyright (C) 2018 University of Dundee & Open Microscopy Environment.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.openmicroscopy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Build JDBC queries for MAPR data service.
 * @author m.t.b.carroll@dundee.ac.uk
 */
class JdbcQueryBuilder {

    /* only these database columns may be queried */
    private static final ImmutableSet<String> COLUMNS_TEXT = ImmutableSet.of("name", "value", "ns");
    private static final ImmutableSet<String> COLUMNS_IDS = ImmutableSet.of(
            "annotation", "image", "dataset", "project", "field", "well", "plate", "screen");
    private static final ImmutableSet<String> COLUMNS;  // the union of the above

    static {
        final ImmutableSet.Builder<String> builder = ImmutableSet.builder();
        builder.addAll(COLUMNS_TEXT);
        builder.addAll(COLUMNS_IDS);
        COLUMNS = builder.build();
    }

    private final StringBuilder query = new StringBuilder();

    /* values to insert for JDBC statement positional parameters */
    private int parameterCount = 0;
    private final Map<Integer, String> parametersText = new HashMap<>();
    private final Map<Integer, Long>   parametersId   = new HashMap<>();

    private Integer maxRows = null;

    /**
     * Convert a JSON array of strings to a native list representation.
     * @param array a JSON array of strings, may be {@code null}
     * @return a corresponding list of strings, never {@code null}
     */
    private static List<String> jsonStringsToList(JsonArray array) {
        if (array == null || array.isEmpty()) {
            return Collections.emptyList();
        }
        final List<String> list = new ArrayList<>(array.size());
        for (int index = 0, target = array.size(); index < target; index++) {
            list.add(array.getString(index));
        }
        return list;
    }

    /**
     * Uses the words from a query specification term to append a space-suffixed expression to the JDBC query.
     * As well as column names, recognizes:
     * <tt>asc</tt>,
     * <tt>desc</tt>,
     * <tt>count</tt>,
     * <tt>distinct</tt>,
     * <tt>length</tt>,
     * <tt>lower</tt>.
     * It does not matter in which order the words are supplied.
     * @param term a query specification term
     * @return the name of the database column that the term references
     */
    private String addTerm(String term) {
        final Set<String> words = Sets.newHashSet(Splitter.on(' ').split(term));
        final StringBuilder prefix = new StringBuilder();
        final StringBuilder suffix = new StringBuilder();
        if (words.remove("lower")) {
            prefix.insert(0, "LOWER(");
            suffix.append(')');
        }
        if (words.remove("length")) {
            prefix.insert(0, "LENGTH(");
            suffix.append(')');
        }
        if (words.remove("distinct")) {
            prefix.insert(0, "DISTINCT ");
        }
        if (words.remove("count")) {
            prefix.insert(0, "COUNT(");
            suffix.append(')');
        }
        if (words.remove("asc")) {
            suffix.append(" ASC");
        }
        if (words.remove("desc")) {
            suffix.append(" DESC");
        }
        if (words.size() != 1) {
            throw new IllegalArgumentException("failed to parse: " + term);
        }
        final String word = words.iterator().next();
        if (!COLUMNS.contains(word)) {
            throw new IllegalArgumentException("failed to parse: " + word);
        }
        query.append(prefix);
        query.append(word);
        query.append(suffix);
        query.append(' ');
        /* it would be more useful to instead return the datatype of the added term */
        return word;
    }

    /**
     * Parses the nestable <q>where</q> specification to append a space-suffixed expression to the JDBC query.
     * Sub-specifications may be listed after an <tt>and</tt> or <tt>or</tt> term. Otherwise the form must be a triple:
     * <dl>
     * <dt>term</dt><dd>as for {@link #addTerm(java.lang.String)}</dd>
     * <dt>relation</dt><dd>such as <tt>in</tt>, <tt>is</tt>, <tt>not</tt>, <tt>!=</tt>, <tt>&lt;</tt>,
     * may bear an optional <tt>not</tt> prefix</dd>
     * <dt>values</dt><dd>a list of literal values, if empty then taken as being {@code null}</dd>
     * </dl>
     * @param where the <q>where</q> specification from the query
     */
    private void addWhereTerms(JsonArray where) {
        final String operator = where.getString(0);
        if ("and".equals(operator) || "or".equals(operator)) {
            /* logical terms can nest: starting "and"/"or" here joins following subterms with that operator */
            boolean separator = false;
            query.append('(');
            for (int index = 1, target = where.size(); index < target; index++) {
                if (separator) {
                    query.append(operator.toUpperCase());
                    query.append(' ');
                } else {
                    separator = true;
                }
                addWhereTerms(where.getJsonArray(index));
            }
            query.insert(query.length() - 1, ')');
        } else {
            /* if not "and"/"or" then must be a term-relation-values triple */
            if (where.size() != 3) {
                throw new IllegalArgumentException("failed to parse: " + where.encode());
            }
            final String term = where.getString(0);
            String relation = where.getString(1);
            final JsonArray values = where.getJsonArray(2);
            final String column = addTerm(term);
            final boolean isNot = relation.startsWith("not ");
            if (isNot) {
                relation = relation.substring(4);
            }
            if ("in".equals(relation) || "is".equals(relation) || "like".equals(relation)) {
                // is okay
            } else {
                for (final char character : relation.toCharArray()) {
                    if ("!<>=".indexOf(character) < 0) {
                        throw new IllegalArgumentException("failed to parse: " + where.encode());
                    }
                }
            }
            if (isNot) {
                query.append("NOT ");
            }
            query.append(relation.toUpperCase());
            if (isNot && "is".equals(relation)) {
                /* SQL syntax irregularity */
                query.setLength(query.length() - 6);
                query.append("IS NOT");
            }
            query.append(' ');
            if (values.isEmpty()) {
                query.append("NULL ");
            } else {
                query.append('(');
                for (int index = 0, target = values.size(); index < target; index++) {
                    query.append("?, ");
                    /* note: bravely assumes that term modifiers like "count" did not change the expected datatype */
                    if (COLUMNS_TEXT.contains(column)) {
                        parametersText.put(++parameterCount, values.getString(index));
                    } else if (COLUMNS_IDS.contains(column)) {
                        parametersId.put(++parameterCount, values.getLong(index));
                    } else {
                        throw new IllegalArgumentException("failed to parse: " + where.encode());
                    }
                }
                query.setCharAt(query.length() - 2, ')');
            }
        }
    }

    /**
     * Construct a JDBC query. The query specification recognizes keys whose values are:
     * <dl>
     * <dt>select</dt><dd><q>select</q> terms (mandatory), see {@link #addTerm(java.lang.String)}</dd>
     * <dt>where</dt><dd><q>where</q> nested term (mandatory), see {@link #addWhereTerms(io.vertx.core.json.JsonArray)}</dd>
     * <dt>group</dt><dd><q>group by</q> terms (optional)</dd>
     * <dt>order</dt><dd><q>order by</q> terms (optional)</dd>
     * <dt>limit</dt><dd>a single integer (optional but recommended)</dd>
     * </dl>
     * @param specification the query represented in JSON
     */
    JdbcQueryBuilder(JsonObject specification) {
        final List<String> select = jsonStringsToList(specification.getJsonArray("select"));
        final JsonArray where = specification.getJsonArray("where");
        final List<String> group = jsonStringsToList(specification.getJsonArray("group"));
        final List<String> order = jsonStringsToList(specification.getJsonArray("order"));
        if (select.isEmpty()) {
            throw new IllegalArgumentException("no terms selected");
        }
        query.append("SELECT ");
        for (final String term : select) {
            addTerm(term);
            query.insert(query.length() - 1, ',');
        }
        query.deleteCharAt(query.length() - 2);
        query.append("FROM _mapr_containers ");
        if (!(where == null || where.isEmpty())) {
            query.append("WHERE ");
            addWhereTerms(where);
        }
        if (!group.isEmpty()) {
            query.append("GROUP BY ");
            for (final String term : group) {
                addTerm(term);
                query.insert(query.length() - 1, ',');
            }
            query.deleteCharAt(query.length() - 2);
        }
        if (!order.isEmpty()) {
            query.append("ORDER BY ");
            for (final String term : order) {
                addTerm(term);
                query.insert(query.length() - 1, ',');
            }
            query.deleteCharAt(query.length() - 2);
        }
        query.setLength(query.length() - 1);
        maxRows = specification.getInteger("limit");
        if (maxRows != null && maxRows < 0) {
            throw new IllegalArgumentException("cannot set negative row limit");
        }
    }

    /**
     * Prepare a JDBC statement object corresponding to this instance's query.
     * @param connection the JDBC connection for which to prepare the query statement
     * @return the prepared query statement ready for execution
     * @throws SQLException if the statement preparation failed
     */
    PreparedStatement createStatement(Connection connection) throws SQLException {
        final PreparedStatement statement = connection.prepareStatement(query.toString());
        for (final Map.Entry<Integer, String> parameter : parametersText.entrySet()) {
            statement.setString(parameter.getKey(), parameter.getValue());
        }
        for (final Map.Entry<Integer, Long> parameter : parametersId.entrySet()) {
            statement.setLong(parameter.getKey(), parameter.getValue());
        }
        if (maxRows != null) {
            statement.setMaxRows(maxRows);
        }
        return statement;
    }
}
